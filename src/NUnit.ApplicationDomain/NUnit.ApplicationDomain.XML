<?xml version="1.0"?>
<doc>
    <assembly>
        <name>NUnit.ApplicationDomain</name>
    </assembly>
    <members>
        <member name="T:NUnit.ApplicationDomain.Internal.ResolveHelper">
            <summary> Helps to resolve the types in another app domain. </summary>
            <remarks>
             The methods are invoked and marshaled from the test app domain into the original domain.
            </remarks>
        </member>
        <member name="T:NUnit.ApplicationDomain.Internal.SetupAndTeardownMethods">
            <summary> The setup and teardown methods to invoke before running a test. </summary>
        </member>
        <member name="M:NUnit.ApplicationDomain.Internal.SetupAndTeardownMethods.#ctor(System.Collections.Generic.IEnumerable{System.Reflection.MethodBase},System.Collections.Generic.IEnumerable{System.Reflection.MethodBase})">
            <summary> Constructor. </summary>
            <exception cref="T:System.ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            <param name="setupMethods"> The setup methods for the current test. </param>
            <param name="teardownMethods"> The teardown methods for the current test. </param>
        </member>
        <member name="P:NUnit.ApplicationDomain.Internal.SetupAndTeardownMethods.SetupMethods">
            <summary> The setup methods for the current test. </summary>
        </member>
        <member name="P:NUnit.ApplicationDomain.Internal.SetupAndTeardownMethods.TeardownMethods">
            <summary> The teardown methods for the current test. </summary>
        </member>
        <member name="T:NUnit.ApplicationDomain.Internal.Utils">
            <summary> Utility methods. </summary>
        </member>
        <member name="M:NUnit.ApplicationDomain.Internal.Utils.GetMethodsWithAttribute``1(System.Type)">
            <summary>
             Get all methods in the type's hierarchy that have the designated attribute.
            </summary>
            <returns>
             Returns methods further down in the type hierarchy first, followed by each subsequent type's
             parents' methods.
            </returns>
        </member>
        <member name="M:NUnit.ApplicationDomain.Internal.Utils.CreateInstanceAndUnwrap``1(System.AppDomain)">
            <summary> Create an instance of the object in the given domain. </summary>
            <param name="domain"> The domain in which the object should be constructed. </param>
            <typeparam name="T"> The type of the object to construct </typeparam>
            <returns> An instance of T, unwrapped from the domain. </returns>
        </member>
        <member name="T:NUnit.ApplicationDomain.Internal.InDomainAssemblyResolver">
            <summary>
             Resolves assemblies by delegating to the parent app domain for assembly locations.
            </summary>
            <remarks> Runs in the test app domain. </remarks>
        </member>
        <member name="M:NUnit.ApplicationDomain.Internal.InDomainAssemblyResolver.#ctor(NUnit.ApplicationDomain.Internal.ResolveHelper)">
            <summary>
             Creates an assembly resolver for all assemblies which might not be in the same path as the
             NUnit.ApplicationDomain assembly.
            </summary>
            <remarks>
             Although this object is created in the original app domain, it is serialized/copied into the
             test app domain and thus all methods except the constructor are invoked in the test domain.
            </remarks>
            <param name="resolveHelper"> The resolve helper from the parent app domain. </param>
        </member>
        <member name="M:NUnit.ApplicationDomain.Internal.InDomainAssemblyResolver.ResolveEventHandler(System.Object,System.ResolveEventArgs)">
            <inheritdoc />
        </member>
        <member name="T:NUnit.ApplicationDomain.Internal.CurrentArgumentsRetriever">
            <summary>
             Facilitates retrieving <code>NUnit.Core.TestExecutionContext.CurrentContext.arguments</code>
            </summary>
        </member>
        <member name="M:NUnit.ApplicationDomain.Internal.CurrentArgumentsRetriever.GetTestArguments(NUnit.Framework.Interfaces.ITest)">
            <summary>
             Attempts to get the arguments for the current executing test by using reflection to get at  
             <code>NUnit.Core.TestExecutionContext.CurrentContext.arguments</code>
            </summary>
            <param name="test"> Gets the tests that were passed into method for the given test. </param>
            <returns> The current arguments for the test, or null of none are available. </returns>
        </member>
        <member name="M:NUnit.ApplicationDomain.Internal.CurrentArgumentsRetriever.GetTestFixtureArguments(NUnit.Framework.Interfaces.ITest)">
            <summary>
             Gets the test-fixture arguments that were used to construct the test fixture associated with
             the given test.
            </summary>
            <param name="test"> Gets the tests that were passed into the test fixture for the given test. </param>
            <returns>
             The arguments used to construct the test fixture, or null if no arguments were used.
            </returns>
        </member>
        <member name="M:NUnit.ApplicationDomain.Internal.CurrentArgumentsRetriever.FindFixture(NUnit.Framework.Interfaces.ITest)">
            <summary> Finds the test fixture associated with the given test. </summary>
        </member>
        <member name="T:NUnit.ApplicationDomain.Internal.ParentAppDomainRunner">
            <summary> Runs a TestMethodInformation in a child app domain. </summary>
        </member>
        <member name="F:NUnit.ApplicationDomain.Internal.ParentAppDomainRunner.CachedInfo">
            <summary> The setup/teardown methods that have been cached for each type thus far. </summary>
        </member>
        <member name="M:NUnit.ApplicationDomain.Internal.ParentAppDomainRunner.#cctor">
            <summary> Static constructor. </summary>
        </member>
        <member name="M:NUnit.ApplicationDomain.Internal.ParentAppDomainRunner.Run(NUnit.Framework.Interfaces.ITest)">
            <summary> Runs the given test for the given type under a new, clean app domain. </summary>
            <exception cref="T:System.ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            <exception cref="T:System.ArgumentException"> Thrown when one or more arguments have unsupported or
             illegal values. </exception>
            <param name="test"> The test that should be run in another app-domain. </param>
            <returns>
             The exception that occurred while executing the test, or null if no exception was generated.
            </returns>
        </member>
        <member name="M:NUnit.ApplicationDomain.Internal.ParentAppDomainRunner.GetSetupTeardownMethods(System.Type)">
            <summary> Gets the setup and teardown methods for the given type. </summary>
            <param name="typeUnderTest"> The type under test. </param>
            <returns>
             The setup teardown methods, loaded from the cache if it already existed, otherwise queried
             via reflection.
            </returns>
        </member>
        <member name="M:NUnit.ApplicationDomain.Internal.ParentAppDomainRunner.GetPermissionSet">
            <summary>
            create a permission set
            </summary>
        </member>
        <member name="T:NUnit.ApplicationDomain.Internal.TestMethodInformation">
            <summary> All of the arguments for the TestExecutor. </summary>
        </member>
        <member name="M:NUnit.ApplicationDomain.Internal.TestMethodInformation.#ctor(System.Type,System.Reflection.MethodBase,NUnit.ApplicationDomain.Internal.SetupAndTeardownMethods,NUnit.Framework.SharedDataStore,System.Object[],System.Object[])">
            <summary> Constructor. </summary>
            <exception cref="T:System.ArgumentNullException"> When one or more required arguments are null. </exception>
            <exception cref="T:System.ArgumentException"> Thrown when one or more arguments have unsupported or
             illegal values. </exception>
            <param name="typeUnderTest"> The type that the method belongs to and which will be
             instantiated in the test app domain. </param>
            <param name="testMethod"> The method to invoke as the core unit of the test. </param>
            <param name="methods"> The setup and teardown methods to invoke before/after running the test. </param>
            <param name="dataStore"> The data store to install into the test AppDomain. </param>
            <param name="testArguments"> The arguments to pass into the test method. </param>
            <param name="testFixtureArguments"> The arguments to use when constructing the test fixture. </param>
        </member>
        <member name="P:NUnit.ApplicationDomain.Internal.TestMethodInformation.TypeUnderTest">
            <summary>
             The name of the class that contains the method to run in the application domain.
            </summary>
        </member>
        <member name="P:NUnit.ApplicationDomain.Internal.TestMethodInformation.MethodUnderTest">
            <summary> Gets or sets the name of the test. </summary>
        </member>
        <member name="P:NUnit.ApplicationDomain.Internal.TestMethodInformation.Arguments">
            <summary>
             Any additional parameters to give to the test method, normally set via TestCaseAttribute.
            </summary>
        </member>
        <member name="P:NUnit.ApplicationDomain.Internal.TestMethodInformation.FixtureArguments">
            <summary>
             Parameters to give to the test constructor, normally set via TestFixtureAttribute.
            </summary>
        </member>
        <member name="P:NUnit.ApplicationDomain.Internal.TestMethodInformation.Methods">
            <summary> The setup and teardown methods to invoke before/after running the test. </summary>
        </member>
        <member name="P:NUnit.ApplicationDomain.Internal.TestMethodInformation.DataStore">
            <summary>  The data store to install into the test AppDomain. </summary>
        </member>
        <member name="P:NUnit.ApplicationDomain.Internal.TestMethodInformation.AppConfigFile">
            <summary> The app config file for the test. </summary>
        </member>
        <member name="P:NUnit.ApplicationDomain.Internal.TestMethodInformation.OutputStream">
            <summary> System.Out. </summary>
        </member>
        <member name="P:NUnit.ApplicationDomain.Internal.TestMethodInformation.ErrorStream">
            <summary> System.Err. </summary>
        </member>
        <member name="M:NUnit.ApplicationDomain.Internal.TestMethodInformation.FindConfigFile(System.Reflection.Assembly)">
            <summary> Try to get the AppConfig file for the assembly. </summary>
            <param name="assembly"> The assembly whose app config file should be retrieved. </param>
            <returns> The path to the config file, or null if it does not exist. </returns>
        </member>
        <member name="T:NUnit.ApplicationDomain.Internal.InDomainTestMethodRunner">
            <summary> Executes a test method in the application domain. </summary>
            <returns> Runs in the test app domain. </returns>
        </member>
        <member name="M:NUnit.ApplicationDomain.Internal.InDomainTestMethodRunner.Execute(NUnit.ApplicationDomain.Internal.TestMethodInformation)">
            <summary> Executes the test method indicates by <paramref name="testMethodInfo"/>. </summary>
            <param name="testMethodInfo"> Information that describes the test method to execute. </param>
            <returns> The exception that occurred as a result of executing the method. </returns>
        </member>
        <member name="M:NUnit.ApplicationDomain.Internal.InDomainTestMethodRunner.RunSetupAndTest(NUnit.ApplicationDomain.Internal.TestMethodInformation,System.Object)">
            <summary> Runs the setup and test method, returning the exception that occurred or null if no exception was fired. </summary>
        </member>
        <member name="M:NUnit.ApplicationDomain.Internal.InDomainTestMethodRunner.CreateAsyncTestResultHandler(System.Object)">
            <summary>
             Creates the <see cref="T:NUnit.ApplicationDomain.IAsyncTestResultHandler"/> to handle an
             async test result.
            </summary>
        </member>
        <member name="M:NUnit.ApplicationDomain.Internal.InDomainTestMethodRunner.RunTeardown(NUnit.ApplicationDomain.Internal.TestMethodInformation,System.Object)">
            <summary> Run each teardown method, returning the first exception that occurred, if any. </summary>
        </member>
        <member name="T:NUnit.ApplicationDomain.IAsyncTestResultHandler">
            <summary>
             Api that allows the task result of async methods to be handled via whatever means a framework
             would like.
            </summary>
        </member>
        <member name="M:NUnit.ApplicationDomain.IAsyncTestResultHandler.Process(System.Threading.Tasks.Task)">
            <summary>
             Invoked when a test that returns a task has been executed. Should not return until the "task"
             is considered complete.
            </summary>
            <param name="task"> The task that the test method returned. </param>
        </member>
        <member name="T:NUnit.ApplicationDomain.TaskWaitTestResultHandler">
            <summary>
             A <see cref="T:NUnit.ApplicationDomain.IAsyncTestResultHandler"/> that simply invokes <see cref="M:System.Threading.Tasks.Task.Wait"/> on the
             result of a task-returning test.
            </summary>
        </member>
        <member name="M:NUnit.ApplicationDomain.TaskWaitTestResultHandler.Process(System.Threading.Tasks.Task)">
            <inheritdoc />
        </member>
        <member name="T:NUnit.Framework.AppDomainRunner">
            <summary> Helps to run a test in another application domain. </summary>
        </member>
        <member name="F:NUnit.Framework.AppDomainRunner.TestAppDomainName">
            <summary> The name of the app-domain in which tests are run. </summary>
        </member>
        <member name="M:NUnit.Framework.AppDomainRunner.#cctor">
            <summary> Static constructor. </summary>
        </member>
        <member name="P:NUnit.Framework.AppDomainRunner.IsInTestAppDomain">
            <summary>
             Returns true if the current test is being executed in an application domain created by the
             <see cref="T:NUnit.Framework.RunInApplicationDomainAttribute"/>
            </summary>
        </member>
        <member name="P:NUnit.Framework.AppDomainRunner.IsNotInTestAppDomain">
            <summary>
             Returns false if the current test is being executed in an application domain created by the
             <see cref="T:NUnit.Framework.RunInApplicationDomainAttribute"/>
            </summary>
            <remarks> Equivalent to !IsInTestAppDomain. </remarks>
        </member>
        <member name="P:NUnit.Framework.AppDomainRunner.ShouldIncludeAppDomainErrorMessages">
            <summary>
             True if messages should be printed to standard output when a test failure occurs while in the
             test app domain. 
            </summary>
            <remarks> True by default. </remarks>
        </member>
        <member name="P:NUnit.Framework.AppDomainRunner.DataStore">
            <summary>
             Properties that are carried into the app-domain and are carried out when the test is over.
            </summary>
        </member>
        <member name="P:NUnit.Framework.AppDomainRunner.HiddenDataStore">
            <summary> The fake data-store set before the test runs. </summary>
        </member>
        <member name="T:NUnit.Framework.RunInApplicationDomainAttribute">
            <summary> Indicates that a test should be run in a separate application domain. </summary>
        </member>
        <member name="M:NUnit.Framework.RunInApplicationDomainAttribute.BeforeTest(NUnit.Framework.Interfaces.ITest)">
            <inheritdoc />
        </member>
        <member name="M:NUnit.Framework.RunInApplicationDomainAttribute.RunInApplicationDomain(NUnit.Framework.Interfaces.ITest)">
            <summary>
             Check if we're in the "test" appdomain, and if we aren't, run the given test in an appdomain,
             capture the result, and propagate it back.
            </summary>
        </member>
        <member name="P:NUnit.Framework.RunInApplicationDomainAttribute.Targets">
            <inheritdoc />
        </member>
        <member name="T:NUnit.Framework.SharedDataStore">
            <summary>
             An object whose properties are stored between a test in the normal app-domain and the test
             executing in the test-domain.
            </summary>
        </member>
        <member name="M:NUnit.Framework.SharedDataStore.Get``1(System.String)">
            <summary> Gets an object with the given key. </summary>
            <typeparam name="T"> The type of object to retrieve. </typeparam>
            <param name="key"> The key of the object to retrieve. </param>
            <returns> An object. </returns>
        </member>
        <member name="M:NUnit.Framework.SharedDataStore.TryGet``1(System.String,``0@)">
            <summary> Attempts to get the given item. </summary>
            <typeparam name="T"> The type of data to retrieve. </typeparam>
            <param name="key"> The key of the object to retrieve. </param>
            <param name="value"> [out] The value or default(T) if the object was not in the data-store. </param>
            <returns>
             True if the value was present and is contained within <paramref name="value"/>, false
             otherwise.
            </returns>
        </member>
        <member name="M:NUnit.Framework.SharedDataStore.Set``1(System.String,``0)">
            <summary> Sets the given value for the given key. </summary>
            <exception cref="T:System.ArgumentException"> Thrown when one or more arguments have unsupported or
             illegal values. </exception>
            <typeparam name="T"> The type of value to set. </typeparam>
            <param name="key"> The key of the object to set. </param>
            <param name="value"> The value of the key to set.  The object must be serializable (or if not
             in the test-appdomain, must derive from MarshalByRefObject). </param>
        </member>
    </members>
</doc>
